package com.devcamp.task56a_50.invoice_rest_api;

public class InvoiceItem {
        private String id;
        private String desc;
        private int qty;
        private double unitPrice;
       
        public InvoiceItem(String id, String desc, int qty, double unitPrice) {
            this.id = id;
            this.desc = desc;
            this.qty = qty;
            this.unitPrice = unitPrice;
        }

        public String getId() {
            return id;
        }

        public String getDesc() {
            return desc;
        }

        public int getQty() {
            return qty;
        }

        public void setQty(int qty) {
            this.qty = qty;
        }

        public double getUnitPrice() {
            return unitPrice;
        }

        public void setUnitPrice(double unitPrice) {
            this.unitPrice = unitPrice;
        }
        
        public double getTotal(){
            return this.unitPrice * this.qty;
        }

        @Override
        public String toString(){
            //return "InvoiceItem[id= " +this.id+ " ,desc= " +this.desc+ " ,qty= " +this.qty+ " ,unitPrice= " +this.unitPrice+ " ]";
            return String.format("InvoiceItem[id=%s, desc=%s, qty=%s, unitPrice=%s]", id,desc,qty,unitPrice);
        }

        
}

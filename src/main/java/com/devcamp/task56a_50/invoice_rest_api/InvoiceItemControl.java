package com.devcamp.task56a_50.invoice_rest_api;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InvoiceItemControl {
    @CrossOrigin
    @GetMapping("/invoices")
    public ArrayList<InvoiceItem> getListInvoiceItem(){
        InvoiceItem hoaDon1 = new InvoiceItem("HD001","Tivi", 2, 7800000.0);
        InvoiceItem hoaDon2 = new InvoiceItem("HD002","Maygiat", 3, 9050000.0);
        InvoiceItem hoaDon3 = new InvoiceItem("HD003","Quat", 12, 800000.0);

        System.out.println(hoaDon1.toString());
        System.out.println(hoaDon2.toString());
        System.out.println(hoaDon3.toString());

        ArrayList<InvoiceItem> invoiceItems = new ArrayList<>();
        invoiceItems.add(hoaDon1);
        invoiceItems.add(hoaDon2);
        invoiceItems.add(hoaDon3);

        return invoiceItems;
    }
}

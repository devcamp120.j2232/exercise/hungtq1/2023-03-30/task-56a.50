package com.devcamp.task56a_50.invoice_rest_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InvoiceRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(InvoiceRestApiApplication.class, args);
	}

}
